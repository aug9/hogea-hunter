#!/bin/env perl 
use strict;
use warnings;
use File::Path;
 
my $dir_name = 0;

while ( $dir_name <= 255 ) {
	$_ = sprintf ("%02x", $dir_name);
	mkpath("imgs/$_");
	mkpath("thumbs/$_");
	$dir_name++;
}
