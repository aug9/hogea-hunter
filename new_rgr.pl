#!/usr/bin/env perl
use strict;
use warnings;
use Digest::MD5;
use File::Copy;
use File::Type;
use WWW::Mechanize;
use autodie;
use DBI;
use GD::Thumbnail;
use Data::Dump;

#if ( -e 'rgr.pid' ) {
#	exit;
#} else {
#	open FH, ">", 'rgr.pid';
#	print FH $$;
#	close FH;
#}
################################################################################
# DB 접속 설정
my $dbh = DBI->connect(
	'dbi:mysql:database=jellydb',
	'jellypo',
	'QhdksJellyPo',
	{ RaiseError => 1, PrintError => 0, AutoCommit => 0 },
);

################################################################################
# 알지롱 환경 설정
my $id = 'jellypo';
my $passwd = 'rg3830mk';
my $bbs_id = 'rgr201306';

# 로그인 주소
my $login = 'http://rgrong.kr/bbs/login.php?id=' . "$bbs_id" . '&page=1&sn1=&divpage=36&sn=on&ss=on&sc=on&select_arrange=headnum&desc=asc&s_url=%2Fbbs%2Fzboard.php%3Fid%3Drgr';

# 로그아웃 주소
my $logout = 'http://rgrong.kr/bbs/logout.php?id=' . "$bbs_id" . '&page=1&sn1=&divpage=37&sn=on&ss=on&sc=on&select_arrange=headnum&desc=asc&s_url=%2Fbbs%2Fzboard.php%3Fid%3Drgr%26club_id%3D';
my $hash_old = "hashdb.old";
my $indexfile = "imgs/index.html";

################################################################################
# 프로그램 시작
################################################################################
#time_p();
print "Start!!\n";
my $mech = WWW::Mechanize->new();
rg_login();

my ( $start_page_num, $last_page_num ) = @ARGV;
$start_page_num ||= start_num();
$last_page_num  ||= last_num();
my $dir = './';


for my $current_page_num ( $start_page_num .. $last_page_num ) {
	print "current page : $current_page_num" . "\n";
	my $g_name = sprintf(
		'http://rgrong.kr/bbs/zboard.php?id=' . "$bbs_id" . '&select_arrange=no&desc=asc&page=%s',
		$current_page_num,
	);
	my $links = get_image_links($g_name);
	if ( $links ) {
		download($links);
	}
}

#unlink 'rgr.pid' or warn "could not remove : $!";
#time_p();
print "End!\n";
$dbh->disconnect();
rg_logout();

#==============================================================================
# 함수 모음
#==============================================================================
# 호게 페이지 열어서 이미지 있는 게시물 링크만 넘겨 받음
# 넘겨받은 링크에서 게시물 번호만 얻어다 예전거랑 비교. 크면 저장
# 같거나 작으면 @links에 넣지 않음.
# 이러면 자연스럽게 예전에 읽은거 또 읽지 않음.
sub get_image_links {
	my ($url) = @_;
	my @links;
	my $response;
	my $rg_no;

	# 파일에서 게시물 번호 얻어오기
	my $no = "last_no.conf";
	open my $last_FH, "<", $no;
	my $no_num = <$last_FH>;
	close $last_FH;

	$mech->get($url); 
	$response = $mech->content;

	my @urls = $response =~ m/a href=\"(.*\d+?)\".*image_up\.gif/g;


	foreach my $link ( @urls ) {
		$rg_no = $link;
		$rg_no =~ s,.*no=(\d+?),$1,g;

		#time_p ();
		print "rg_no : $rg_no\tno_num $no_num\n";
		if ( $rg_no > $no_num ) {
			$link = 'http://rgrong.kr/bbs/' . "$link";
			next unless $link =~ /view\.php/;
			push @links, $link;
		}
	}

	my $last_end = $links[-1];
	if ( $last_end ) {
		$last_end =~ s,.*no=(\d+),$1,g;
		open my $FH, ">", $no;
		print $FH "$last_end";
		#time_p();
		print "last_end changed : $last_end\n";
		close $FH;
	}
	return \@links;
}

sub download {
	my ($links) = @_;

	open my $FH_old, ">>", "$hash_old";
	for my $article_link ( @{$links} ) {
		my $rg_no = $article_link;
		$rg_no =~ s,.*no=(\d+),$1,g;
		my $res;

#		$mech->add_header( Referer => undef );

		$mech->get($article_link); 
		$res = $mech->content;
		my @img_urls = $res =~ m|(data.?/.*?)\"|g; #"
		my @filenames;
		foreach my $img_url ( @img_urls ) {
			my $img_link = "http://rgrong.kr/bbs/$img_url";

			# 확장자 얻어오기
			my $ext = $img_url;
			$ext =~ s,.*\.(.*)?$,$1,g;

			my $res;
			eval { $res = $mech->get($img_link); };
			if ($@) {
				warn $@;
				next;
			}
			my $md5 = Digest::MD5->new();
			$md5->add($res->content);
			my $md5_name = $md5->hexdigest;


			# 최종 파일명 얻었음 == $file_name
			# complete/$1/$1$2 로 있는지 체크해서 없으면 저장
			# 있으면 dup에 기록 후 다음으로!
			my $file_name = "$md5_name.$ext";
			my $dir = $file_name;
			$dir =~ s,^(..).*,$1,g;
			#time_p();
			print "file exist check, \./imgs/$dir/$file_name\n";

			# 파일이 있든 없든 관련 정보 기록
			my $sql = "insert into $bbs_id ( no, name ) values ( \"$rg_no\", \"$file_name\")";
			my $sth = $dbh->prepare($sql);
			$sth->execute or die "$DBI::errstr\n";
			$sth->finish();


			# 파일 있는지 체크
			if ( -e "\./imgs/$dir/$file_name" ) {
				#time_p();
				print "$rg_no $file_name \tis exist\n";
			} else {
				# 썸네일 생성
				eval {
					my $t = GD::Thumbnail->new;
					my $raw = $t->create( $res->content, 140, 0 );
					open FH, ">", "thumbs/$dir/$file_name";
					binmode FH;
					print FH $raw;
					close FH;
				};
				if ( $@ ) {
					warn $@;
					next;
				}
				#
				# 그림 파일 저장
				open my $FH, ">", "imgs/$dir/$file_name";
				binmode $FH;
				print $FH $res->content;
				close $FH;

				print $FH_old "$file_name\n";
				# html 파일에 쓰기
				push @filenames, "$dir/$file_name";

				#sql 작성
				my $sql_time = time_p();
				$sql = "insert into md5_list ( saveTime, name ) values ( \"$sql_time\", \"$file_name\")";
				$sth = $dbh->prepare($sql);
				$sth->execute or die "$DBI::errstr\n";
				$sth->finish();

			}
		}
		#create_html( @filenames );
	}
	close $FH_old;
}

sub create_html {
	my @filenames = @_;
	while ( @filenames ) {
		# html 파일 번호 구해서 파일명 만듦
		open my $FH, "<", 'last_html.conf';
		my $last_html = <$FH>;
		$last_html ||= 0;
		chomp $last_html;
		close $FH;
		my $htmlfile = "imgs/all_$last_html.html";;

		my $file_count;
		# html 파일명 만든걸 불러와서 줄 수 불러옮
		# 줄 수 40개가 안되면 @new_files 내용을 적음
		my $FH_html;
		if ( -e $htmlfile ) {
			open $FH_html, "<", $htmlfile;
			1 while <$FH_html>;
			$file_count = $.;
			close $FH_html;
		} else 	{
			# 새로운 html 생성할 때, index 파일에 링크 생성.
			$file_count = 0;
			open my $FH_idx, ">>", $indexfile;
			my $indextag = $htmlfile;
			$indextag =~ s,imgs/(.*),<a href=$1>$1</a>,g;
			print $FH_idx "$indextag\n";
			close $FH_idx;
		}

		# 그림 파일 태그를 html에 작성하는 부분
		open $FH_html, ">>", $htmlfile;
		for ( ; $file_count < 40 ; $file_count++ ) {
			my $pic = shift @filenames;
			if ( $pic ) {
				chomp $pic;
				# all_NNNN.html 파일 만듦
				# hashdb.old에 파일 이름 넣음
				my $pic_tag = $pic;
				$pic_tag =~ s,^(.*)$,<img src=$1><br>,g;

				#time_p();
				print "$htmlfile\tcount $file_count\tpic $pic\n";
				print $FH_html "$pic_tag\n";

				# 다음 그림 파일 이름을 넣음, 

				# pic이 비어있으면 그림 파일이 더 없는 것이므로 종료
				unless ( @filenames ) {
					return;
				}
			}
		}
		close $FH_html;

# 여기까지 왔으면 html에 40개 넘게 썼는데도 @new_files에 파일 목록이 있는 경우,
# $last_html 숫자 늘려주고 파일에 저장
		if ( $file_count == 40 ) {
			++$last_html;
			#time_p();
			print "last_html $last_html\n";
			open $FH, ">", 'last_html.conf';
			print $FH "$last_html";
			close $FH;
		}
	}
}

# 알지롱 로그인
sub rg_login {
	$mech->get( $login );

	$mech->submit_form(
		form_name => 'login',
		fields    => {
			user_id => $id,
			password => $passwd
		},
	);
}


# 알지롱 로그오프
sub rg_logout {
	$mech->get ( $logout );
}

sub start_num {
	my $readfile = "last_num.conf";
	open my $FH, "<", $readfile;
	my $num = <$FH>;
	close $FH;
	return $num;
}

sub last_num {
	my $writefile = "last_num.conf";
	$mech->get("http://rgrong.kr/bbs/zboard.php?id=$bbs_id"); 
	my $res = $mech->content;

	my @nums = $res =~ m|\&nbsp;(\d+)\&nbsp;\&nbsp;|g;

	# 작업 시작 페이지랑 종료 페이지가 같으면 쿨하게 프로그램 종료
	if ( $start_page_num == "$nums[-1]" ) {
	} else {
		open my $lastFH, ">", $writefile;
		print $lastFH "$nums[-1]";
		close $lastFH;
	}

	return "$nums[-1]";
}

sub time_p {
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
	my $localtime = ($year + 1900) . "-" . ($mon + 1) . "-" .  $mday . " " . $hour . ":" . $min . ":" . $sec;
	return $localtime;
}
